# Horários IFPB

O sistema de controle de horários do Intituto Federal da Paraíba é uma plataforma voltada principalmente o gerenciamento dos horários pelos administradores e coordenadores da instituição, porém para o público geral também há funções como ver os horários de disciplinas, professores e salas de aula.

# Pré requisitos

- Ruby 2.4.1
- Ruby on Rails 5.1
- PostgreSQL >= 9.4

# Recomendações

Nós estamos utilizando o Editorconfig para padronizar o ambiente de desenvolvimento, é interessante dar uma olhada neste site (editorconfig.org).

